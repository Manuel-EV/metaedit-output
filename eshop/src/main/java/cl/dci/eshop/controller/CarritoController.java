package cl.dci.eshop.controller;
import cl.dci.eshop.model.Carrito;
import cl.dci.eshop.model.Producto;
import cl.dci.eshop.model.ProductoCarrito;
import cl.dci.eshop.auth.User;
import cl.dci.eshop.repository.CarritoRepository;
import cl.dci.eshop.repository.ProductoCarritoRepository;
import cl.dci.eshop.repository.ProductoRepository;

	import org.springframework.beans.factory.annotation.Autowired;
	import org.springframework.security.access.prepost.PreAuthorize;
	import org.springframework.security.core.context.SecurityContextHolder;
	import org.springframework.stereotype.Controller;
	import org.springframework.ui.Model;
	import org.springframework.web.bind.annotation.*;
	
	
	import java.util.List;
	


			@Controller
			@RequestMapping("/api/carrito")
			public class CarritoController{
			

		@Autowired
		private CarritoRepository carritoRepository;
		

		@Autowired
		private ProductoCarritoRepository productoCarritoRepository;
		

		@Autowired
		private ProductoRepository productoRepository;
		

		@PreAuthorize("hasAuthority('carrito:manage')")
		 @PostMapping("/crear/{id}")
		
		public String agregarProducto(@PathVariable int id){
			
Producto producto = productoRepository.findById(id).orElse(null);
Carrito carrito = getCurrentUser().getCarrito();
carrito.addProducto(producto);          ProductoCarrito pc = new ProductoCarrito(carrito, producto);         productoCarritoRepository.save(pc);
carritoRepository.save(carrito);


return "redirect:/carrito";
		}
		

		@PreAuthorize("hasAuthority('carrito:manage')")
		@PostMapping(path = "{id}")
		
		public String eliminarProducto(@PathVariable int id){
			Carrito carrito = getCurrentUser().getCarrito();
ProductoCarrito pc = productoCarritoRepository.findById(id).orElse(null);         Producto producto = pc.getProducto();          carrito.deleteProducto(producto);          productoCarritoRepository.delete(pc);
carritoRepository.save(carrito);


return "redirect:/carrito";
		}
		

		
		
		
		 User getCurrentUser(){
			Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();         User user = null;          if (principal instanceof User) {             user = ((User) principal);         }         return user;



		}
		

			}