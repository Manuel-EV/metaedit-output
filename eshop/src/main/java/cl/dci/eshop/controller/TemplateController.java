
	package cl.dci.eshop.controller;

import cl.dci.eshop.auth.User;
import cl.dci.eshop.model.Carrito;
import cl.dci.eshop.model.Producto;
import cl.dci.eshop.model.ProductoCarrito;
import cl.dci.eshop.repository.CarritoRepository;
import cl.dci.eshop.repository.ProductoCarritoRepository;
import cl.dci.eshop.repository.ProductoRepository;
import cl.dci.eshop.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
	@Controller
		@RequestMapping("/")
		public class TemplateController {
		    @Autowired
    private ProductoRepository productoRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private CarritoRepository carritoRepository;
    @Autowired
    private ProductoCarritoRepository productoCarritoRepository;


        @GetMapping()
        public String getRoot() {
            return "redirect:/home";
        }

		    @GetMapping("admin/pedidos")
    public String getAdminPedidos(Model modelo) {  basicSetup(modelo, "AdminPedidos");

        return "admin/pedidos";
    }
		@PreAuthorize("hasAnyRole('ROLE_ADMIN')")
		    @GetMapping("admin/productos")
    public String getAdminProductos(Model modelo) {  basicSetup(modelo, "AdminProductos");modelo.addAttribute("producto", new Producto());  modelo.addAttribute("productos", productoRepository.findAll());

        return "admin/admin-productos";
    }
		@PreAuthorize("hasAnyRole('ROLE_ADMIN')")
		    @GetMapping("admin/usuarios")
    public String getAdminUsuarios(Model modelo) {  basicSetup(modelo, "AdminUsuarios"); modelo.addAttribute("usuario", new User());  modelo.addAttribute("usuarios", userRepository.findAll());

        return "admin/admin-usuarios";
    }
		@PreAuthorize("hasAuthority('carrito:manage')")
		    @GetMapping("carrito")
    public String getCarrito(Model modelo) {  basicSetup(modelo, "Carrito");Carrito carrito = getCurrentUser().getCarrito();          modelo.addAttribute("carrito", carrito);         modelo.addAttribute("prodCars", getProductoCarritos());

        return "carrito";
    }
		
		    @GetMapping("catalogo")
    public String getCatalogo(Model modelo) {  basicSetup(modelo, "Catalogo");modelo.addAttribute("productos", productoRepository.findAll());

        return "catalogo";
    }
		
		    @GetMapping("home")
    public String getHome(Model modelo) {  basicSetup(modelo, "Home");

        return "home";
    }
		
		    @GetMapping("login")
    public String getLogin() {  

        return "login";
    }
		@PreAuthorize("hasAuthority('perfil:manage')")
		    @GetMapping("perfil")
    public String getPerfil(Model modelo) {  basicSetup(modelo, "Perfil");
            User user = getCurrentUser();
            modelo.addAttribute("usuario", user);
        return "perfil";
    }
		
		    @GetMapping("producto/{idProducto}")
    public String getProducto(@PathVariable("idProducto") Integer idProducto, Model modelo) {  basicSetup(modelo, "Producto");Producto producto = productoRepository.findById(idProducto).orElse(null);
modelo.addAttribute("producto", producto);   

        return "producto";
    }
		
		    @GetMapping("registro")
    public String getRegistro(Model modelo) {
                modelo.addAttribute("titulo", "Registro");
                modelo.addAttribute("usuario", new User());
        return "registro";
    }
private Model basicSetup(Model modelo, String titulo){
        modelo.addAttribute("titulo", titulo);
        modelo.addAttribute("usuarioLogueado", this.usuarioLogueado());
        String rol = usuarioLogueado() ? getCurrentUser().getRole().name():"";
        modelo.addAttribute("rolUsuario", rol);

        return modelo;
    }

private boolean usuarioLogueado() {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return !principal.toString().equals("anonymousUser");
    }
private List<ProductoCarrito> getProductoCarritos(){
        Carrito carrito = getCurrentUser().getCarrito();

        return productoCarritoRepository.findByCarrito(carrito);
    }

   private User getCurrentUser() {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User user = null;

        if (principal instanceof User) {
            user = ((User) principal);
        }
        return user;
    }


}

