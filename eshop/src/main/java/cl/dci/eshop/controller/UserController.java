package cl.dci.eshop.controller;
import cl.dci.eshop.model.Carrito;
import cl.dci.eshop.auth.User;
import cl.dci.eshop.repository.CarritoRepository;
import cl.dci.eshop.repository.UserRepository;
import static cl.dci.eshop.security.ApplicationUserRole.*;
		import org.springframework.security.crypto.password.PasswordEncoder;
		
	import org.springframework.beans.factory.annotation.Autowired;
	import org.springframework.security.access.prepost.PreAuthorize;
	import org.springframework.security.core.context.SecurityContextHolder;
	import org.springframework.stereotype.Controller;
	import org.springframework.ui.Model;
	import org.springframework.web.bind.annotation.*;
	
	
	import java.util.List;
	
import static cl.dci.eshop.security.ApplicationUserRole.*;  import org.springframework.security.crypto.password.PasswordEncoder;

			@Controller
			@RequestMapping("/api/usuario")
			public class UserController{
			private final PasswordEncoder passwordEncoder;

		@Autowired
		private CarritoRepository carritoRepository;
		

		@Autowired
		private UserRepository userRepository;
		

		@PreAuthorize("hasAuthority('usuario:write')")
		@PostMapping("/crear")
		
		public String crearUsuario(@ModelAttribute("usuario") User usuario){
			
Carrito carrito = new Carrito();             carrito.setUser(usuario);        String password = passwordEncoder.encode(usuario.getPassword());        usuario.setPassword(password);         userRepository.save(usuario);       carritoRepository.save(carrito);
return "redirect:/admin/usuarios";
		}
		

		
		@PostMapping("/registrar")
		
		public String registrarUsuario(@ModelAttribute("usuario") User usuario){
			
Carrito c1 = new Carrito();         c1.setUser(usuario);         System.out.println("USUARIO: "+usuario);         String password = passwordEncoder.encode(usuario.getPassword());         usuario.setPassword(password);         usuario.setRole(CUSTOMER);         userRepository.save(usuario);         carritoRepository.save(c1);
return "redirect:/login";
		}
		

		
		
		@Autowired
		  UserController(PasswordEncoder passwordEncoder){
			
this.passwordEncoder = passwordEncoder;

		}
		

			}