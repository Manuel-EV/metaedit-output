package cl.dci.eshop.model;
import cl.dci.eshop.auth.User;


	import lombok.*;

	import javax.persistence.*;
	import java.util.*;
	
	@Entity @Table(name = "carrito") @Data
public class Carrito{
@Column
private int cantidadProductos;
@Id  @GeneratedValue(strategy = GenerationType.AUTO) @Column(name = "carrito_id")
private int id;
@Column
private int precioTotal;
@OneToMany(mappedBy = "carrito")
private List<ProductoCarrito> productoCarritos;
@OneToOne()     @JoinTable(name = "user_carrito",             joinColumns = {@JoinColumn(name = "carrito_id")},             inverseJoinColumns = {@JoinColumn(name = "user_id")})
private User user;
public Carrito(){
this.cantidadProductos = 0;         this.precioTotal = 0;         this.productoCarritos = new ArrayList<>();
}
public void addProducto(Producto producto){
this.precioTotal += producto.getPrecio();         this.cantidadProductos++;
}
public void deleteProducto(Producto producto){
this.precioTotal -= producto.getPrecio();         this.cantidadProductos--;
}

}