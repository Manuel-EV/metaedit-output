package cl.dci.eshop.model;

	import lombok.*;

	import javax.persistence.*;
	import java.util.*;
	
	@Entity @Data @Table(name = "producto")
public class Producto{
@Id @GeneratedValue(strategy = GenerationType.AUTO)
private int id;
@Column
private String nombre;
@Column
private int precio;
@OneToMany(mappedBy = "producto", cascade = CascadeType.MERGE)
private List<ProductoCarrito> productoCarritos;
public Producto(){
this.productoCarritos = new ArrayList<>();
}

}