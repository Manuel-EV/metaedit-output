package cl.dci.eshop.model;

	import lombok.*;

	import javax.persistence.*;
	import java.util.*;
	
	@Entity @Table(name = "producto_carrito") @Data @NoArgsConstructor
public class ProductoCarrito{
@ManyToOne @JoinColumn(name = "carrito_id")
private Carrito carrito;
@Id @GeneratedValue(strategy = GenerationType.AUTO)
private int id;
@ManyToOne @JoinColumn(name = "producto_id")
private Producto producto;
public ProductoCarrito(Carrito carrito, Producto producto){
this.producto = producto;         this.carrito = carrito;
}

}