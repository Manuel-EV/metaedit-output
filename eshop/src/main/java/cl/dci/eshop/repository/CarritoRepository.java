
package cl.dci.eshop.repository;

import cl.dci.eshop.model.*;
import org.springframework.data.jpa.repository.JpaRepository;
import java.util.*;


public interface CarritoRepository extends JpaRepository<Carrito, Integer> {

}