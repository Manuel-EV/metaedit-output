
package cl.dci.eshop.repository;

import cl.dci.eshop.model.*;
import org.springframework.data.jpa.repository.JpaRepository;
import java.util.*;

import cl.dci.eshop.auth.User; import java.util.Optional;
public interface UserRepository extends JpaRepository<User, Integer> {
Optional<User> findByUsername(String username);
}