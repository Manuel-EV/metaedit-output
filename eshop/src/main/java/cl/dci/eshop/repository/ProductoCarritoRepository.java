
package cl.dci.eshop.repository;

import cl.dci.eshop.model.*;
import org.springframework.data.jpa.repository.JpaRepository;
import java.util.*;


public interface ProductoCarritoRepository extends JpaRepository<ProductoCarrito, Integer> {
List<ProductoCarrito> findByCarrito(Carrito carrito); List<ProductoCarrito> findByProducto(Producto producto);
}